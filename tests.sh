#!/bin/bash

# detect current directory
BASEDIR=`dirname $0`
BASEDIR=`(cd "$BASEDIR"; pwd)`

(
    cd $BASEDIR/tests
    python2 -m unittest -v all
    python3 -m unittest -v all
    #python -m unittest -v all
)
