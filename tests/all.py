#!/usr/bin/env python
# coding=utf8
#

import os, sys, subprocess
import unittest

CURRENT_PATH  = os.path.dirname(os.path.abspath(__file__))
PROJECTS_PATH = os.path.join(CURRENT_PATH, "projects")
ZM_BIN        = os.path.join(CURRENT_PATH, "../zm.py")

class TestSimple(unittest.TestCase):

    def setUp(self):
        pass

    def testBuildConfig(self):
        #print("123")
        self.assertEqual(1, 1)

        print("\n")
        os.chdir(CURRENT_PATH)
        subprocess.call([ZM_BIN, "b", "Simple"])

if __name__ == '__main__':
    unittest.main()
