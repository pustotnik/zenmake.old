#!/usr/bin/env python
# coding=utf8
#

import sys

if sys.hexversion < 0x020600f0:
	raise ImportError('Python >= 2.6 is required')

def main():

    from zm.controller import App
    app = App()
    if app.run(sys.argv):
        return 0
    return 1

if __name__ == '__main__':
    sys.exit(main())
