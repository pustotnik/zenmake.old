#!/usr/bin/env python
# coding=utf8
#

"""
some binding with waf
"""

import os, sys
from zm.constant import ZENMAKE_DIR

WAF_DIRNAME         = "waf"
WAF_DIR             = os.path.join(ZENMAKE_DIR, WAF_DIRNAME)

sys.path.insert(0, WAF_DIR)
from waflib.Context import WAFVERSION

WAF_VERSION         = WAFVERSION

from waflib import Utils, Logs

#def logDebug(*k, **kw):
    #Logs.warn(*k, **kw)

#def logError(*k, **kw):
    #Logs.error(*k, **kw)
