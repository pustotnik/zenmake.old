#!/usr/bin/env python
# coding=utf8
#

# special dict

class AutoDict(dict):

    def __init__(self):
        self.parent = None

    def __missing__(self, key):
        return AutoDict()

    def __getattr__(self, name):
        if name == 'parent':
            return super(AutoDict, self).__getattr__(name)
        else:
            if name not in self:
                self[name] = AutoDict()
                self[name].parent = self
            return self[name]

    def __setattr__(self, name, value):
        if name == 'parent':
            super(AutoDict  , self).__setattr__(name, value)
        else:
            self[name] = value
