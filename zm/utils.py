#!/usr/bin/env python
# coding=utf8
#

import os, inspect
joinpath  = os.path.join
abspath   = os.path.abspath

from zm.constant import ZENMAKE_CONF

def makepath(arg, *args):
    """
    Join one or more path components and make a normalized absolutized version
    """
    #if arg.startswith(curdir):
    #    # detect right 'current' directory path for build scripts
    #
    #    callerFrame = inspect.stack()[1]
    #    # each record is a tuple of six items: the frame object, the filename,
    #    # the line number of the current line, the function name, a list of lines
    #    # of context from the source code, and the index of the current line within that list
    #    fullPath = callerFrame[1]
    #    if basename(fullPath) == ZENMAKE_CONF:
    #        arg = joinpath(dirname(fullPath), arg)

    # TODO: add conversion from posix path to current OS path
    return abspath( joinpath(arg, *args) )

def logError(msg):
    # TODO: color error message
    print(msg)
