#!/usr/bin/env python
# coding=utf8
#

import os, sys
import auxiliary.argparse as argparse

from zm.constant import ZENMAKE_VERSION, ZENMAKE_DIR, ZENMAKE_CONF

class App(object):
    def __init__(self):
        pass

    def run(self, cmdLineArgs):

        ###############################################################
        # GLOBAL

        class MyHelpFormatter(argparse.HelpFormatter):
            def __init__(self, prog):
                super(MyHelpFormatter, self).__init__(prog,
                    max_help_position = 27)

        appName = cmdLineArgs[0]
        parser = argparse.ArgumentParser(prog = appName,
            formatter_class = MyHelpFormatter,
            description = 'ZenMake: build system',
            #epilog = 'use "%s help" for the full list of commands' % appName,
            usage = "%(prog)s <command> [options] [args]",
            add_help = False)

        groupGlobal = parser.add_argument_group('global options')

        groupGlobal.add_argument('-h', '--help', action='help',
            help = "show this help message and exit")
        groupGlobal.add_argument('-v', '--version', action='version',
            help = "show program's version number and exit",
            version = ZENMAKE_VERSION)

        subparsers = parser.add_subparsers(
            title='list of commands',
            help = '', metavar = '',
            dest='command')

        ###############################################################
        # COMMANDS

        commandObjs  = {}
        commandHelps = {}
        for zmCmd in self._getCommands():
            if zmCmd == 'help': # will be processed later
                continue

            zmCmd = __import__('zm.command.' + zmCmd, fromlist=['Self'])
            cmd = zmCmd.Self()

            commandObjs[cmd.name] = cmd
            for alias in cmd.aliases:
                commandObjs[alias] = cmd

            commandHelps[cmd.name] = {}
            cmdHelpInfo = commandHelps[cmd.name]
            cmdHelpInfo['help']     = cmd.description
            cmdHelpInfo['usage']    = "%s %s" % (appName, cmd.usageText)
            cmdHelpInfo['aliases']  = cmd.aliases

            cmdParser = subparsers.add_parser(cmd.name,
                help = cmdHelpInfo['help'],
                usage = cmdHelpInfo['usage'],
                aliases = cmdHelpInfo['aliases'],
                add_help = False)

            groupGlobal = cmdParser.add_argument_group('global options')
            groupGlobal.add_argument('-h', '--help', action='help',
                help = "show this help message and exit")

            groupCmdOpts = cmdParser.add_argument_group('command options')

            if cmd.needConfig:
                groupCmdOpts.add_argument('-c', '--config', metavar = 'zmconfig',
                    type = str,
                    help = "path to build config (" + ZENMAKE_CONF + ")")

            if cmd.needProject:
                cmdParser.add_argument('project')
            cmdParser.set_defaults(func = cmd.handler)
            cmdHelpInfo['help'] = cmdParser.format_help()


        # special case for 'help' command
        from zm.command.help import Self as HelpCmd
        cmd = HelpCmd(commandHelps)
        commandObjs['help'] = cmd
        for alias in cmd.aliases:
                commandObjs[alias] = cmd

        cmdParser = subparsers.add_parser(cmd.name, add_help = True,
            help = cmd.description,
            usage = "%s %s" % (appName, cmd.usageText))

        # for correct work of this command hash 'commandHelps' must be full
        commandHelps['help'] = {}
        cmdHelpInfo = commandHelps['help']
        cmdHelpInfo['overview'] = parser.format_help()
        cmdHelpInfo['help']     = cmd.description
        cmdHelpInfo['usage']    = "%s %s" % (appName, cmd.usageText)
        cmdHelpInfo['aliases']  = cmd.aliases
        # we must recreate this command for correct work of command handler
        cmd = HelpCmd(commandHelps)
        cmdParser.set_defaults(func = cmd.handler)

        # parse args and run command
        args = None
        selectedCommand = None
        if(len(cmdLineArgs) < 2): # hack for show help if command is not defined
            args = parser.parse_args(['-h'])
        else:
            args, argsUnknown = parser.parse_known_args(cmdLineArgs[1:])

            # hack for force argparse to do what I want (default value of 'topic') :)
            commandName = args.command
            selectedCommand = commandObjs[commandName]
            args = cmdLineArgs[1:]
            if commandName == 'help':
                cmdParser.add_argument('topic')
                if not argsUnknown:
                    args.append('overview')

            args = parser.parse_args(args)

        if selectedCommand is not None:
            if selectedCommand.needConfig:
                # read all configs at once
                from zm.configreader import ConfigReader
                ConfigReader(args.config).load()

        # as selectedCommand.handler(args) except case when command is not defined
        return args.func(args)

    def _getCommands(self):
        dirPath = os.path.join(ZENMAKE_DIR, 'zm', 'command')
        ignoredNames = ('base.py', "__init__.py")
        return map (lambda f: f[:-3],
            filter(lambda f: f.endswith(".py") and f not in ignoredNames,
                os.listdir(dirPath)))
