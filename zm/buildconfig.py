# coding=utf8
#

# default config for build project

import os
from zm.config import *

"""
Root path for directory with buid configs (zmconfig.py). Can be None,
path (not only directory but path to zmconfig.py), list of such paths.
Optional. Used for auto finding of all configs
"""
settings.config.searchpath = None
#settings.config.searchpath = makepath(ZENMAKE_DIR, os.path.pardir, "projects")

"""
TODO: add description
Can be "everywhere" or "bypath".
WARNING. Use mode "everywhere" very carefully because it can be very slow for
big directory with many files and directories
"""
#settings.config.searchmode = "everywhere"
settings.config.searchmode = "bypath"

"""
# Example for project setting:

"""
