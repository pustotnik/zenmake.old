#!/usr/bin/env python
# coding=utf8
#

"""
Global constant values
"""

import os, sys

ZENMAKE_NAME        = "ZenMake"
ZENMAKE_VERSION     = "0.0.1"
ZENMAKE_CONF        = "zmconfig.py"

ZENMAKE_DIR         = os.path.dirname(os.path.abspath(sys.argv[0]))

USER_HOMEDIR        = os.path.expanduser('~')

#PURPLE = "\n\033[95m%s\033[0m"
#GRAY   = "\033[90m%s\033[0m"
#RED    = "\033[91m%s\033[0m"
#GREEN  = "\033[92m%s\033[0m"
#COLORS = (PURPLE, GRAY, RED, GREEN)
