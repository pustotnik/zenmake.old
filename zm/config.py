#!/usr/bin/env python
# coding=utf8
#

import os
from zm.autodict import AutoDict
from zm.constant import ZENMAKE_DIR, USER_HOMEDIR
from zm.utils import makepath

joinpath  = os.path.join
abspath   = os.path.abspath
dirname   = os.path.dirname
basename  = os.path.basename

settings  = AutoDict()
supported = AutoDict()

# TODO: see toolset/gcc.py
supported.toolset.gcc.cc = ""

def getconfig(path):
    pass
