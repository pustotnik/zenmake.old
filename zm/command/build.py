#!/usr/bin/env python
# coding=utf8
#

from .base import *

class Self(Command):
    def __init__(self):
        super(Self, self).__init__(
            'build',
            'build project', ('b', 'bld'))

    def _handleCommand(self, args):
        executor = Executor()
        return executor.run(args)

class Executor(BaseExecutor):
    def __init__(self):
        super(Executor, self).__init__()

    def run(self, args):
        from zm.config import settings
        from pprint import pprint
        pprint(settings)
        print("building %s ..." % args.project)
        return True
