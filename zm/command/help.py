#!/usr/bin/env python
# coding=utf8
#

from .base import *

class Self(Command):
    def __init__(self, commandHelps):
        super(Self, self).__init__(
            'help',
            'show help for a given topic or a help overview')
        self._commandHelps = commandHelps

    @property
    def usageText(self):
        return "%s [command/topic]" % self.nameVariantsString

    @property
    def needProject(self):
        return False

    @property
    def needConfig(self):
        return False

    @property
    def commandHelps(self):
        return self._commandHelps

    def _handleCommand(self, args):
        executor = Executor(self)
        return executor.run(args)

class Executor(BaseExecutor):
    def __init__(self, selfCommand):
        super(Executor, self).__init__()
        self._selfCommand = selfCommand;

    def run(self, args):

        commandHelps = self._selfCommand.commandHelps
        topic = args.topic

        if topic == 'overview':
            print(commandHelps['help']['overview'])
            return

        for k in commandHelps:
            if topic in commandHelps[k]['aliases']:
                topic = k
                break

        if topic not in commandHelps:
            print("Unknown command/topic: '%s'" % topic)
        else:
            print(commandHelps[topic]['help'])

        return True
