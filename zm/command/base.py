#!/usr/bin/env python
# coding=utf8
#

class Command(object):
    def __init__(self, name, description, aliases = ()):
        self._name        = name
        self._description = description
        self._aliases     = aliases

    @property
    def name(self):
        return self._name

    @property
    def description(self):
        return self._description

    @property
    def aliases(self):
        return self._aliases

    @property
    def usageText(self):
        return "%s [options] {project name}" % self.nameVariantsString

    @property
    def nameVariantsString(self):
        cmdVariants = self.name
        for alias in self._aliases:
            cmdVariants += "|%s" % alias
        return cmdVariants

    @property
    def needProject(self):
        return True

    @property
    def needConfig(self):
        return True

    @property
    def handler(self):
        return self._handleCommand

    def _handleCommand(self, args):
        raise NotImplementedError

class BaseExecutor(object):
    def __init__(self):
        pass
