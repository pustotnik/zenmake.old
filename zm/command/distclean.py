#!/usr/bin/env python
# coding=utf8
#

from .base import *

class Self(Command):
    def __init__(self):
        super(Self, self).__init__(
            'distclean',
            'removes the build directory with everything in it', ('D',))

    @property
    def needProject(self):
        return False

    def _handleCommand(self, args):
        executor = Executor()
        return executor.run(args)

class Executor(BaseExecutor):
    def __init__(self):
        super(Executor, self).__init__()

    def run(self, args):
        print("distclean ...")
        return True
