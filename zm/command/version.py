#!/usr/bin/env python
# coding=utf8
#

from .base import *

class Self(Command):
    def __init__(self):
        super(Self, self).__init__(
            'version',
            'output version and copyright information', ('v',))

    @property
    def usageText(self):
        return "%s" % self.nameVariantsString

    @property
    def needProject(self):
        return False

    @property
    def needConfig(self):
        return False

    def _handleCommand(self, args):
        executor = Executor()
        return executor.run(args)

class Executor(BaseExecutor):
    def __init__(self):
        super(Executor, self).__init__()

    def run(self, args):
        from zm.constant import ZENMAKE_NAME, ZENMAKE_VERSION
        from zm.wafbinding import WAF_VERSION
        print(ZENMAKE_NAME + ' ' + ZENMAKE_VERSION + \
            ' (used waf ' + WAF_VERSION +')')
        return True
