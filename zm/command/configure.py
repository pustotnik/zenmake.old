#!/usr/bin/env python
# coding=utf8
#

from .base import *

class Self(Command):
    def __init__(self):
        super(Self, self).__init__(
            'configure',
            'configure project', ('cfg', 'conf'))

    def _handleCommand(self, args):
        executor = Executor()
        return executor.run(args)

class Executor(BaseExecutor):
    def __init__(self):
        super(Executor, self).__init__()

    def run(self, args):
        print("configuration ...")
        return True
