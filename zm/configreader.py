#!/usr/bin/env python
# coding=utf8
#

import os, sys, imp
from itertools import *
from zm.utils import makepath, logError

join      = os.path.join
abspath   = os.path.abspath
dirname   = os.path.dirname
basename  = os.path.basename
fexists   = os.path.exists
splitext  = os.path.splitext
curdir    = os.path.curdir
parentdir = os.path.pardir

from zm.constant import ZENMAKE_CONF, ZENMAKE_DIR, USER_HOMEDIR

class ConfigReaderError(Exception):
    pass

class ConfigReader(object):

    def __init__(self, cmdlineConfigPath):
        # Dictionary holding already loaded modules, keyed by their absolute path.
        self._modules = {}
        self._cmdlineConfigPath = cmdlineConfigPath
        self._cwdPath = os.getcwd()

    def load(self):
        """
        Load all configs.
        """

        """
один проект - много целей
зависимости по целям
имя проекта это его путь
при поиске конфига по пути проекта искать по очереди в каждой части пути начиная сверху
нет понятия версия на уровне путей, все есть путь
зависимости могут быть по разным целям в том смысле что либа может быть собрана CLANG, а конечная цель gcc
        """

        rootPath = abspath(os.sep)

        # fisrst load default config
        self._loadConfig(makepath(ZENMAKE_DIR, "zm", "buildconfig.py"))

        if self._cmdlineConfigPath:
            self._cmdlineConfigPath = makepath(self._cmdlineConfigPath)
            if not fexists(self._cmdlineConfigPath):
                logError("Config file '" + self._cmdlineConfigPath + "' is not exists")
                return

            self._loadConfig(self._cmdlineConfigPath)
            return

        defaultPaths = \
        (
            # /etc/zenmake/zmconfig.py
            makepath(rootPath, 'etc', 'zenmake', ZENMAKE_CONF),
            # ~/.config/zmconfig.py
            makepath(USER_HOMEDIR, '.confg', ZENMAKE_CONF),
            # ~/zmconfig.py
            makepath(USER_HOMEDIR, ZENMAKE_CONF),
            # ./zmconfig.py
            makepath(self._cwdPath, ZENMAKE_CONF),
        )

        for confPath in defaultPaths:
            if fexists(confPath):
                self._loadConfig(confPath)

    def _loadConfig(self, confPath):

        if self._modules.has_key(confPath):
            return

        cfg = self._loadConfigModule(confPath)
        self._fixKnownConfPathParams(cfg, confPath)

        configSearchPath = cfg.settings.config.searchpath
        if not configSearchPath:
            return

        if isinstance(configSearchPath, basestring):
            configSearchPath = (configSearchPath, )

        # need to reset for each config
        cfg.settings.config.searchpath = None

        searchmode = cfg.settings.config.searchmode

        # find and read all from settings.config.searchpath for each config
        for path in configSearchPath:
            if basename(path) == ZENMAKE_CONF and fexists(path):
                self._loadConfig(makepath(path))
                continue

            if searchmode == "everywhere":
                self._loadAllConfigsFromDir(path)
            elif searchmode == "bypath":
                pass
            else:
                logError("Unknown search mode '" + searchmode + "'")

    def _loadAllConfigsFromDir(self, dirPath):

        # TODO: read in threads ?

        def walkOnError(err):
            logError(err)

        #for dirpath, _, filenames in os.walk(dirPath, followlinks = True):
        # realpath
        for dirpath, _, filenames in os.walk(dirPath, onerror = walkOnError):
            for fileName in filter(lambda f: f == ZENMAKE_CONF, filenames):
                self._loadConfig(makepath(dirpath, fileName))

    def _fixKnownConfPathParams(self, confModule, confPath):

        confDir  = dirname(confPath)
        settings = confModule.settings

        # config search path
        if settings.config.searchpath:
            if isinstance(settings.config.searchpath, basestring):
                settings.config.searchpath = \
                    self._fixConfPathParam(confDir, settings.config.searchpath)
            else:
                settings.config.searchpath = \
                    tuple(imap(lambda x: self._fixConfPathParam(confDir, x),
                        settings.config.searchpath))

        def fixEachProject(param):
            # project path
            for prjId, prj in param.iteritems():
                if prj.has_key("path"):
                    prj.path = self._fixConfPathParam(confDir, prj.path)
                else:
                    if len(prj) == 0:
                        raise ConfigReaderError("Unknown path for project '" + prjId + "'")
                    else:
                        fixEachProject(prj)

        fixEachProject(settings.project)

    def _fixConfPathParam(self, confDir, param):
        if param.startswith(curdir):
            param = makepath(confDir, param)
        return param

    def _loadConfigModule(self, path):
        """
        Load a source file as a python module.
        """

        module = self._modules.get(path)
        if module:
            return module

        #print(path)

        # Get module name without file extension
        moduleName = splitext(os.path.basename(path))[0]

        module = imp.new_module(moduleName)

        from zm.wafbinding import Utils

        try:
            code = Utils.readf(path, m='rU', encoding = None)
        except EnvironmentError:
            raise Exception('Could not read the file %r' % path)

        moduleDir = dirname(path)
        sys.path.insert(0, moduleDir)

        # special injection
        module.__dict__['configdir'] = moduleDir

        os.chdir(moduleDir)
        try:
            exec(compile(code, path, 'exec'), module.__dict__)
        finally:
            os.chdir(self._cwdPath)

        sys.path.remove(moduleDir)

        self._modules[path] = module

        return module
